package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.exception.NotFoundException;
import com.ictcampus.lab.base.control.model.WorldRequest;
import com.ictcampus.lab.base.control.model.WorldResponse;
import com.ictcampus.lab.base.service.WorldService;
import com.ictcampus.lab.base.service.model.World;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;



@RestController
@RequestMapping( "/api/v1/worlds" )
@AllArgsConstructor
@Slf4j
public class WorldController {
	@Autowired
	private WorldService worldService;

	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<WorldResponse> getWorlds(
			@RequestParam( required = false ) String name,
			@RequestParam( required = false ) String search
	) {
		List<WorldResponse> worldResponses = new ArrayList<>();

		/*for ( World world : worldService.getWorlds( name, search ) ) {
			worldResponses.add( convertToWorldResponse( world ) );
		}
		return worldResponses;*/

		return worldService.getWorlds(name, search)
				.stream()
				.map( item -> convertToWorldResponse( item ) )
				.collect( Collectors.toList());
	}

	@GetMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public WorldResponse getWorld( @PathVariable( name = "id" ) Long id ) throws NotFoundException {
		World world = worldService.getWorldById( id );
		if ( world == null ) {
			log.info( "Pianeta con ID [{}] non trovato", id );
			throw new NotFoundException();
		}

		WorldResponse worldResponse = convertToWorldResponse( world );
		log.info( "Restituisco il pianeta con ID [{}] e dati [{}]", id, worldResponse );
		return worldResponse;
	}

	@PostMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public Long createWorld(
			@RequestBody WorldRequest worldRequest
	) {
		log.info( "Creo un nuovo pianeta con i dati [{}]", worldRequest );

		return worldService.addWorld( worldRequest.getName(), worldRequest.getSystem() );
	}

	@PutMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void editWorld(
			@PathVariable( name = "id" ) Long id,
			@RequestBody WorldRequest worldRequest
	) {
		log.info( "Aggiorno il pianeta con ID [{}] e dati [{}]", id, worldRequest );
		worldService.editWorld( id, worldRequest.getName(), worldRequest.getSystem() );
	}

	@DeleteMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public void deleteWorld(
			@PathVariable( name = "id" ) Long id
	) {
		log.info( "Cancello il pianeta con ID [{}]", id );
		worldService.deleteWorld( id );
	}

	private WorldResponse convertToWorldResponse( World world ) {
		WorldResponse worldResponse = new WorldResponse();
		worldResponse.setId( world.getId() );
		worldResponse.setName( world.getName() );
		worldResponse.setSystem( world.getSystem() );

		return worldResponse;
	}
}
