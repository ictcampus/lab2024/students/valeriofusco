package com.ictcampus.lab.base.control.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;



public class WorldRequest {
	private String name;
	private String system;

	public String getName() {
		return name;
	}

	public void setName( final String name ) {
		this.name = name;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem( final String system ) {
		this.system = system;
	}

	@Override
	public String toString() {
		return "WorldRequest [name=" + name + ", system=" + system + "]";
	}
}
