package com.ictcampus.lab.base.control;

import com.ictcampus.lab.base.control.model.BookResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;



@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
public class BookController {

	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBooks() {
		List<BookResponse> list = new ArrayList<>();

		BookResponse bookResponse = new BookResponse();
		bookResponse.setId( 1L );
		bookResponse.setTitle( "Titolo del libro di prova" );
		bookResponse.setAuthor( "Autore di Prova" );
		bookResponse.setISDB( "CODICE_ISDB_DI_PROVA" );

		list.add( bookResponse );
		return list;
	}
}
