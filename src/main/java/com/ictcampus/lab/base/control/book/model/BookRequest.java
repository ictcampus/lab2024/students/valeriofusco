package com.ictcampus.lab.base.control.book.model;

import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;


@Data
@Builder
@Jacksonized
public class BookRequest {
    String title;
    String isbn;
    String descAbstract;
    String description;
    String publisher;
    LocalDate publishedDate;
    BigDecimal price;
    BigDecimal discount;
    List<AuthorResponse> authors;
}

