package com.ictcampus.lab.base.control.book.model;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;


@Value
@Builder
@Jacksonized
public class AuthorResponse {
	Long id;
	String name;
	String surname;
	String nickname;
	LocalDate birthday;
}
