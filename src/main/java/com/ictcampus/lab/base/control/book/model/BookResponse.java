package com.ictcampus.lab.base.control.book.model;

import com.ictcampus.lab.base.service.book.model.Author;
import com.ictcampus.lab.base.service.book.model.Image;
import com.ictcampus.lab.base.service.book.model.Position;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Data
@Builder
@Jacksonized
public class BookResponse {
	Long id;
	String title;
	String isbn;
	String descAbstract;
	String description;
	String publisher;
	LocalDate publishedDate;
	BigDecimal price;
	BigDecimal discount;
	List<AuthorResponse> authors;
}
