package com.ictcampus.lab.base.service.user;


import com.ictcampus.lab.base.repository.user.UserRepository;
import com.ictcampus.lab.base.repository.user.entity.UserEntity;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserEntity registerUser(UserEntity userEntity) {
        // Cripta la password in MD5
        userEntity.setPassword(DigestUtils.md5Hex(userEntity.getPassword()));
        return userRepository.save(userEntity);
    }
}