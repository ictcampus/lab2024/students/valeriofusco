package com.ictcampus.lab.base.service.model;

import java.util.ArrayList;
import java.util.List;


public class World {
	private Long id;
	private String name;
	private String system;
	private List<String> moons = new ArrayList<>();

	public World( final Long id, final String name, final String system, final List<String> moons ) {
		this.id = id;
		this.name = name;
		this.system = system;
		this.moons = moons;
	}

	public World( final Long id, final String name, final String system ) {
		this.id = id;
		this.name = name;
		this.system = system;
	}

	public World() {}

	public Long getId() {
		return id;
	}

	public void setId( final Long id ) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName( final String name ) {
		this.name = name;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem( final String system ) {
		this.system = system;
	}

	public List<String> getMoons() {
		return moons;
	}

	public void setMoons( final List<String> moons ) {
		this.moons = moons;
	}

	@Override
	public String toString() {
		return "World{" +
			   "id=" + id +
			   ", name='" + name + '\'' +
			   ", system='" + system + '\'' +
			   ", moons=" + moons +
			   '}';
	}
}
