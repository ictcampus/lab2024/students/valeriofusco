package com.ictcampus.lab.base.service.book.model;

import lombok.Data;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Data
public class Book {
	private Long id;
	private String title;
	private String isbn;
	private String descAbstract;
	private String description;
	private String publisher;
	private LocalDate publishedDate;
	private BigDecimal price;
	private BigDecimal discount;
	private List<Author> authors;	//relazione molti a molti con Autore
	private Image thumbnail;	//relazione 1 a molti con immagine
	private List<Image> images;	//relazione molti a molti con immagine
	private Position position;	//relazione 1 a 1 con posizione


	/*
    public void addAuthor(Author author) {
        if (!this.authors.contains(author)) {
            this.authors.add(author);
            author.addBook(this);  // Aggiunge il libro all'autore mantenendo la bidirezionalità
        }
    }

    public void removeAuthor(Author author) {
        if (this.authors.contains(author)) {
            this.authors.remove(author);
            author.removeBook(this);  // Rimuove il libro dall'autore mantenendo la bidirezionalità
        }
    }
 */


	/*
    public void addImage(Image image) {
        if (!this.images.contains(image)) {
            this.images.add(image);
            image.addBook(this);  // Aggiunge il libro all'immagine mantenendo la bidirezionalità
        }
    }

    public void removeImage(Image image) {
        if (this.images.contains(image)) {
            this.images.remove(image);
            image.removeBook(this);  // Rimuove il libro dall'immagine mantenendo la bidirezionalità
        }
    }
 */
}
