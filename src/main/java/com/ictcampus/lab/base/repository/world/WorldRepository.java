package com.ictcampus.lab.base.repository.world;

import com.ictcampus.lab.base.repository.world.entity.WorldEntity;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



@AllArgsConstructor
@Repository
@Slf4j
public class WorldRepository {
	private JdbcTemplate jdbcTemplate;

	public List<WorldEntity> findAll() {
		return jdbcTemplate.query( "SELECT * FROM world", new BeanPropertyRowMapper<>( WorldEntity.class ) );
	}

	public WorldEntity findById( long id ) {
		return jdbcTemplate.queryForObject( "SELECT * FROM world WHERE id = ?",
				new BeanPropertyRowMapper<>( WorldEntity.class ), id );
	}

	public WorldEntity findByName( String name ) {
		return jdbcTemplate.queryForObject( "SELECT * FROM world WHERE name = ?",
				new BeanPropertyRowMapper<>( WorldEntity.class ), name );
	}

	public List<WorldEntity> findByNameOrSearch( String name, String search ) {

		/*if ( (name == null || name.isEmpty()) && (search == null || search.isEmpty()) ) {
			return findAll();
		}*/

		List<String> args = new ArrayList<>();
		String sql = " SELECT * "
					 + "FROM world "
					 + "WHERE (1=1) ";

		if( name != null && !name.isEmpty() ) {
			sql += " AND (name ILIKE ?) ";
			args.add( "%" + name + "%");
		}

		if( search != null && !search.isEmpty() ) {
			sql += " AND (name ILIKE ? OR system ILIKE ?)";
			args.add( "%" + search + "%");
			args.add( "%" + search + "%");
		}


		log.info( "Ho chiamato findByNameOrSearch() e SQL generato è [{}]", sql );

		return jdbcTemplate.query( sql,
				new BeanPropertyRowMapper<>( WorldEntity.class ), args.toArray( new String[0] ) );
	}

	@Transactional
	public long create( String name, String system ) {
		GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
		PreparedStatementCreatorFactory preparedStatementCreatorFactory = new PreparedStatementCreatorFactory(
				"INSERT INTO world (name, system) VALUES (?, ?)",
				Types.VARCHAR, Types.VARCHAR
		);
		preparedStatementCreatorFactory.setReturnGeneratedKeys( true );

		PreparedStatementCreator preparedStatementCreator = preparedStatementCreatorFactory
				.newPreparedStatementCreator(
						Arrays.asList( name, system )
				);


		jdbcTemplate.update( preparedStatementCreator, generatedKeyHolder );

		return generatedKeyHolder.getKey().longValue();
	}

	public int update( long id, String name, String system ) {
		return jdbcTemplate.update( "UPDATE world SET name=?, system=? WHERE id=?", name, system, id );
	}

	public int delete( long id ) {
		return jdbcTemplate.update( "DELETE FROM world WHERE id=?", id );
	}
}
