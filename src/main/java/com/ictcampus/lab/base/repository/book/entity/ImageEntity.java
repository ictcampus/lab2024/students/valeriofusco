package com.ictcampus.lab.base.repository.book.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.proxy.HibernateProxy;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Data
@Entity
@Table( name = "images" )
public class ImageEntity {
	@Id
	private Long id;

	private String title;
	private String url;

	@Column( name = "is_thumbnail" )
	private boolean thumbnail;

	@ManyToMany( mappedBy = "images" ) @ToString.Exclude
	List<BookEntity> books = new ArrayList<>();

	@OneToMany( mappedBy = "thumbnail", cascade = CascadeType.ALL, fetch = FetchType.LAZY ) @ToString.Exclude
	List<BookEntity> booksWithThumbnail = new ArrayList<>();

}
