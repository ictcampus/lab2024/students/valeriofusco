package com.ictcampus.lab.base.repository.user.entity;

import com.ictcampus.lab.base.repository.book.entity.AuthorEntity;
import com.ictcampus.lab.base.service.user.UserRole;
import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "users") // Specificare il nome della tabella
public class UserEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String username;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String surname;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false)
	private UserRole role;

	@ManyToMany
	@JoinTable(
			name = "user_address",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "address_id")
	) @ToString.Exclude
	private List<AddressEntity> address = new ArrayList<>();
	/*@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<AddressEntity> addresses; // Assicurati di avere una classe AddressEntity mappata correttamente
*/
}