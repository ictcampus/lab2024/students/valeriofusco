package com.ictcampus.lab.base.repository.book;

import com.ictcampus.lab.base.repository.book.entity.AuthorEntity;
import com.ictcampus.lab.base.repository.book.entity.BookEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class BookRepositoryCustomImpl implements BookRepositoryCustom {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<BookEntity> findBooksWithExtendData() {
		String sql = "SELECT book.id AS book_id, book.title AS book_title, " +
				"author.id AS author_id, author.name AS author_name, author.surname AS author_surname " +
				"FROM books book " +
				"JOIN book_author ba ON book.id = ba.book_id " +
				"JOIN authors author ON author.id = ba.author_id ";

		return jdbcTemplate.query(sql, new BookExtractor());
	}

	@Override
	public List<BookEntity> searchBooksByTitleOrAuthor(String searchString) {
		String sql = "SELECT b.id, b.title, a.name AS author_name, a.surname AS author_surname " +
				"FROM books b " +
				"LEFT JOIN book_author ba ON b.id = ba.book_id " +
				"LEFT JOIN authors a ON ba.author_id = a.id " +
				"WHERE LOWER(b.title) LIKE LOWER(?) " +
				"OR LOWER(a.name) LIKE LOWER(?) " +
				"OR LOWER(a.surname) LIKE LOWER(?)";

		String searchParam = "%" + searchString.toLowerCase() + "%";

		return jdbcTemplate.query(sql, new Object[]{searchParam, searchParam, searchParam}, new BookExtractor());
	}

	private static class BookExtractor implements ResultSetExtractor<List<BookEntity>> {

		@Override
		public List<BookEntity> extractData(ResultSet rs) throws SQLException {
			Map<Long, BookEntity> bookMap = new HashMap<>();
			Map<Long, AuthorEntity> authorMap = new HashMap<>();

			while (rs.next()) {
				long bookId = rs.getLong("book_id");
				BookEntity bookEntity = bookMap.get(bookId);
				if (bookEntity == null) {
					bookEntity = new BookEntity();
					bookEntity.setId(bookId);
					bookEntity.setTitle(rs.getString("book_title"));
					bookEntity.setAuthors(new ArrayList<>());
					bookMap.put(bookId, bookEntity);
				}

				long authorId = rs.getLong("author_id");
				AuthorEntity authorEntity = authorMap.get(authorId);
				if (authorEntity == null) {
					authorEntity = new AuthorEntity();
					authorEntity.setId(authorId);
					authorEntity.setName(rs.getString("author_name"));
					authorEntity.setSurname(rs.getString("author_surname"));
					authorEntity.setBooks(new ArrayList<>());
					authorMap.put(authorId, authorEntity);
				}

				bookEntity.getAuthors().add(authorEntity);
				authorEntity.getBooks().add(bookEntity);
			}

			return new ArrayList<>(bookMap.values());
		}
	}
}

