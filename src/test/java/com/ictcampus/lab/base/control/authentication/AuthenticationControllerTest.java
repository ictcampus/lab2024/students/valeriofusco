package com.ictcampus.lab.base.control.authentication;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.crypto.SecretKey;


@Slf4j
class AuthenticationControllerTest {
	private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Test
	@Disabled
	void generateSecretKey() {
		SecretKey secretKey = Keys.secretKeyFor( SignatureAlgorithm.HS256 ); //or HS384 or HS512
		String secretString = Encoders.BASE64.encode(secretKey.getEncoded());
		log.info("Secret Key: [{}]", secretString);
	}

	@Test
	@Disabled
	void createUserPassword() {
		String password = "test";
		String passwordCrypted = passwordEncoder.encode(password);

		log.info( "password: [{}]", passwordCrypted );
	}
}